from django.db import models

class Group(models.Model):
    groupname = models.CharField(max_length=50)
    studentcount = models.IntegerField()    