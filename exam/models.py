from django.db import models

class Exam(models.Model):
    login = models.CharField(max_length=50)
    subjectname = models.CharField(max_length=50)
    theme = models.CharField(max_length=50)
    mark = models.FloatField()
    maks_mark = models.FloatField()
