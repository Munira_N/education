from django.urls import path
from .views import exam

urlpatterns = [
    path('', exam, name='exam')
]