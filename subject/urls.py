from django.urls import path
from .views import subject, subject_create, title_create

urlpatterns = [
    path('', subject, name='subject'),
    path('subject_create', subject_create, name = 'subject_create'),
]