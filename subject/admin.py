from django.contrib import admin

from .models import Subject, Title

admin.site.register(Subject)

admin.site.register(Title)