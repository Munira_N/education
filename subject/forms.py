from django.forms import ModelForm
from .models import Subject

class SubjectCreateForm(ModelForm):
    class Meta:
        model = Subject
        fields = ['subjectName', 'theme',  'author']