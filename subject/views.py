from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from .forms import SubjectCreateForm
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate

def subject(request):
    return render(request,'subject/subject.html')
def subject_create(request):
    if request.method == 'GET':
        return render(request, 'subject/subject_create.html',{'form':SubjectCreateForm})
    else:
        subject_create = SubjectCreateForm(request.POST)
        new_createsubject = subject_create.save(commit = False)
        new_createsubject.author = request.User
        new_createsubject.save()
        return redirect('subject')
def title_create(requst):
    return render(requst, 'subject/title_create,html')