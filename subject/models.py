from django.db import models

from django.contrib.auth.models import User

class Subject(models.Model):
    subjectName = models.CharField(max_length=50)
    theme = models.CharField(max_length=50)
    author = models.ForeignKey(User, on_delete = models.CASCADE)

    def __str__(self):
        return self.author       
class Title(models.Model):
    title = models.ForeignKey(Subject, on_delete = models.CASCADE)
    themedescriptoin = models.TextField()
    materilas = models.URLField(blank=True)

    def __str__(self):
        return self.title