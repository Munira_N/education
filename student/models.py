from django.db import models
from subject.models import Subject
from django.contrib.auth.models import User

class Student(models.Model):
    login = models.CharField(max_length=50)
    lasttime = models.DateField() 
class StudentBlog(models.Model):
    title = models.ForeignKey(Subject, on_delete = models.CASCADE)
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User, on_delete = models.CASCADE)
       
    def __str__(self):
        return self.title