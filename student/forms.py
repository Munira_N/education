from django.forms import ModelForm
from .models import StudentBlog

class BlogCreateForm(ModelForm):
    class Meta:
        model = StudentBlog
        fields = ['title', 'content', 'author']