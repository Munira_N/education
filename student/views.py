from django.shortcuts import render, redirect
from .forms import BlogCreateForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate

def student(request):
    return render(request,'student/student.html')

def student_create(request):
    if request.method == 'GET':
        return render(request, 'student/student_create.html', {'form':BlogCreateForm})
    else:
        student_create = BlogCreateForm(request, 'POST')
        new_createstudent = student_create.save(commit = False)
        new_createstudent.author = request.user
        new_createstudent.save()
        return redirect('student_create')