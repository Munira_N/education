
from django.urls import path
from .views import student, student_create
from student import views

urlpatterns = [
    path('', student, name='student'),
    path('student_create/', views.student_create, name='student_create'),
    ]