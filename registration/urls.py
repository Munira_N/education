from django.urls import path
from .views import registration
from student import views
from registration import views

urlpatterns = [
    path('registration/', registration, name='registration'),
    path('student/', views.student, name = 'student'),
    path('logout/', views.logout_user, name = 'logout_user'),
    path('login/', views.login_user, name = 'login_user'),
    path('', views.home, name= 'home'),
    ]