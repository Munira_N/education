from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate

def registration(request):
    if request.method == 'GET':
        user = UserCreationForm()
        return render(request,'registration/registration.html',{'form':user})
    else:
        if request.POST['password1']==request.POST['password2']:
            try:
                user = User.objects.create_user(request.POST['username'], password = request.POST['password1'])
                user.save()
                login(request, user)
                return redirect('student')
            except IntegrityError:
                return render(request, 'registration/registration.html', {'form':UserCreationForm(),'error':'Bu login bazamizda mavjud :('})
                
            
        else:
            return render(request, 'registration/registration.html', {'form':UserCreationForm(),'error':'Sizning parolingiz bir xil emas'})
def student(request):
    return render(request, 'student/student.html')
def home(request):
    return render(request, 'registration/home.html')
def logout_user(request):
    if request.method == 'POST':
        logout(request)
        return redirect('home')
def login_user(request):
    if request.method == 'GET':
        user = AuthenticationForm()
        return render(request, 'registration/login_user.html',{'form':user})
    else:
        user = authenticate(request, username=request.POST['username'], password = request.POST['password'])
        if user is None:
            return render(request, 'registration/login_user.html', {'form':AuthenticationForm, 'error':'Parol yoki login xato kiritilgan'})
        else:
            login(request, user)    
            return redirect('student')    